package security.app.service.security;

/**
 * Created by Молния on 26.02.2017.
 */
public interface SecurityService {
    String findLoggedInUsername();
    void autoLogin(String username, String password);
}
