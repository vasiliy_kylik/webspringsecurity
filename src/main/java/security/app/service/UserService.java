package security.app.service;

import security.app.model.User;

/**
 * Created by Молния on 26.02.2017.
 */
public interface UserService {
    void save(User user);
    User findByUsername(String username);
}
