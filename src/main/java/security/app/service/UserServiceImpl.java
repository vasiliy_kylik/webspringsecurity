package security.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import security.app.dao.RoleDao;
import security.app.dao.UserDao;
import security.app.model.Role;
import security.app.model.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Молния on 26.02.2017.
 *
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private BCryptPasswordEncoder bcryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bcryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.getOne(1L));
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
