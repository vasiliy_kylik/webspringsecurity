package security.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import security.app.model.Role;

/**
 * Created by Молния on 26.02.2017.
 */
public interface RoleDao extends JpaRepository<Role, Long> {
}
