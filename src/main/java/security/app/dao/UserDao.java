package security.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import security.app.model.User;

/**
 * Created by Молния on 26.02.2017.
 */
public interface UserDao extends JpaRepository<User,Long> {
    User findByUsername(String username);
}
